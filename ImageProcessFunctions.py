import cv2
import numpy as np
import math
import copy
import matplotlib.pyplot as plt
import statistics
import DetectChars
import main2
import Character
import NumberPlate
import possibleplate


GAUSSIAN_SMOOTH_FILTER_SIZE = (5, 5)
ADAPTIVE_THRESH_BLOCK_SIZE = 19
ADAPTIVE_THRESH_WEIGHT = 9

def getGrayImage(imgOriginal):
    return cv2.cvtColor(imgOriginal, cv2.COLOR_BGR2GRAY)
def thresholdBilateral(imgGrayscale):
    noise_removal = cv2.bilateralFilter(imgGrayscale, 85, 75, 75)
    imgGrayscale = cv2.subtract(imgGrayscale, noise_removal)
    _, imgThresh = cv2.threshold(imgGrayscale, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return imgThresh

def thresholdBilateralInverse(imgGrayscale):
    noise_removal = cv2.bilateralFilter(imgGrayscale, 85, 75, 75)
    imgGrayscale = cv2.subtract(imgGrayscale, noise_removal)
    _, imgThresh = cv2.threshold(imgGrayscale, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    return imgThresh
def inverseOtsuThreshold(imgGrayscale):
    _, imgThresh = cv2.threshold(imgGrayscale, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    return imgThresh

def thresholdGaussian(imgGrayscale):
    imgblurred = cv2.GaussianBlur(imgGrayscale, (75, 75), 0)
    imgGrayscale = cv2.subtract(imgGrayscale, imgblurred)
    _, imgThresh = cv2.threshold(imgGrayscale, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return imgThresh
def otsuThreshold(imgGrayscale):
    _, imgThresh = cv2.threshold(imgGrayscale, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    return imgThresh
def preprocess(imgOriginal):
 return
# end function
def invertImage(img):
    return  cv2.bitwise_not(img)

def blurImage(imgGrayscale):
    imgBlurred = cv2.GaussianBlur(imgGrayscale, GAUSSIAN_SMOOTH_FILTER_SIZE, 0)
    return imgBlurred
#end function

def findEdges(img):
    cannyimg = cv2.Canny(img, 70, 80)
    return cannyimg

#end function

def findContours(img):
    contours, npaHierarchy = cv2.findContours(img, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
    return  contours
#end function
def maximizeContrast(imgGrayscale):

    height, width = imgGrayscale.shape

    imgTopHat = np.zeros((height, width, 1), np.uint8)
    imgBlackHat = np.zeros((height, width, 1), np.uint8)

    structuringElement = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    imgTopHat = cv2.morphologyEx(imgGrayscale, cv2.MORPH_TOPHAT, structuringElement)
    imgBlackHat = cv2.morphologyEx(imgGrayscale, cv2.MORPH_BLACKHAT, structuringElement)

    imgGrayscalePlusTopHat = cv2.add(imgGrayscale, imgTopHat)
    imgGrayscalePlusTopHatMinusBlackHat = cv2.subtract(imgGrayscalePlusTopHat, imgBlackHat)

    return imgGrayscalePlusTopHatMinusBlackHat
# end function
def extractValue(imgOriginal):
    height, width, numChannels = imgOriginal.shape

    imgHSV = np.zeros((height, width, 3), np.uint8)

    imgHSV = cv2.cvtColor(imgOriginal, cv2.COLOR_BGR2HSV)

    imgHue, imgSaturation, imgValue = cv2.split(imgHSV)

    return imgValue
# end function

def deBlurr(imgGrayscale):
    kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]])
    imgGrayscale = cv2.filter2D(imgGrayscale, -1, kernel)
    return imgGrayscale

