import numpy as np
import cv2
import sys
import time
import random
import DetectedVehicle

class YOLO:
    def __init__(self):
        self.labelsPath = "yolo-coco/coco.names"
        self.LABELS = open(self.labelsPath).read().strip().split("\n")
        self.weightsPath  = "yolo-coco/yolov3.weights"
        self.configPath = "yolo-coco/yolov3.cfg"
        self.confidence = 0.5
        self.net = None
        self.ln = None
        self.classes = ['car', 'motorbike', 'bus', 'truck']
        self.load_net()

    def load_net(self):
        print("Loading Network..")
        self.net = cv2.dnn.readNetFromDarknet(self.configPath, self.weightsPath)
        self.net.setPreferableBackend(cv2.dnn.DNN_BACKEND_OPENCV)
        # To set to gpu mode
        # self.net.setPreferableTarget(cv2.dnn.DNN_TARGET_OPENCL)
        self.ln = self.net.getLayerNames()
        print(self.ln)
        self.ln = [self.ln[i[0] - 1] for i in self.net.getUnconnectedOutLayers()]
        print("Network Loaded")


    def return_predict(self, image):

        print('[INFO] Starting prediction of image')
        start = time.time()
        (H, W) = image.shape[:2]
        blob = cv2.dnn.blobFromImage(image, 1 / 255.0, (416, 416),
                                     swapRB=True, crop=False)
        self.net.setInput(blob)
        layerOutputs = self.net.forward(self.ln)
        result = self.process_output(layerOutputs, W, H)
        print('[INFO] Done predicting for image. TOOK ' + str(int(time.time() - start)) + 'secs')
        return result

    def process_output(self,layerOutputs, W, H):
        boxes = []
        confidences = []
        classIDs = []

        for output in layerOutputs:
            # loop over each of the detections
            for detection in output:

                # extract the class ID and confidence (i.e., probability) of
                # the current object detection
                scores = detection[5:]
                classID = np.argmax(scores)
                confidence = scores[classID]

                # filter out weak predictions by ensuring the detected
                # probability is greater than the minimum probability
                if confidence > 0.5:
                    # scale the bounding box coordinates back relative to the
                    # size of the image, keeping in mind that YOLO actually
                    # returns the center (x, y)-coordinates of the bounding
                    # box followed by the boxes' width and height
                    box = detection[0:4] * np.array([W, H, W, H])
                    (centerX, centerY, width, height) = box.astype("int")

                    # use the center (x, y)-coordinates to derive the top and
                    # and left corner of the bounding box
                    x = int(centerX - (width / 2))
                    y = int(centerY - (height / 2))

                    # update our list of bounding box coordinates, confidences,
                    # and class IDs
                    boxes.append([x, y, int(width), int(height)])
                    confidences.append(float(confidence))
                    classIDs.append(classID)

        nmsRes = cv2.dnn.NMSBoxes(boxes, confidences, 0.2, 0.4)
        return self.return_objects(nmsRes, boxes, confidences, classIDs)

    def return_objects(self, nmsRes, boxes, confidences, classIDs):
        objects = []
        if len(nmsRes) > 0:
            # loop over the indexes we are keeping
            for i in nmsRes.flatten():
                # extract the bounding box coordinates
                object = {
                    "label" : self.LABELS[classIDs[i]],
                    "confidence" : confidences[i],
                    "tlx" : boxes[i][0],
                    "tly" : boxes[i][1],
                    "width" : boxes[i][2],
                    "height" : boxes[i][3]
                }
                if object['label'] in self.classes:
                    objects.append(object)

        return objects


    def start_detecting(self):
        camera = cv2.VideoCapture(0)
        camera.set(cv2.CAP_PROP_FRAME_WIDTH, 500)
        camera.set(cv2.CAP_PROP_FRAME_HEIGHT, 350)

        ret, frame = camera.read()
        print(self.return_predict(frame))

    def detect_image(self, img):
        objects = self.return_predict(img)
        color =(1,255,1)
        detectedVehicleList = []

        for object in objects:
            x1= object['tlx']
            x2= object['tlx'] + object['width']
            y1= object['tly']
            y2= object['tly'] + object['height']

            if(x1<0):
                x1 = 0
            if (x2<0):
                x2=0
            if(y1<0):
                y1=0
            if(y2<0):
                y2=0
            points = [x1,y1,x2,y2]
            tempimg = img[y1:y2,x1:x2]
            dv= DetectedVehicle.DetectedVehicle(tempimg,points)
            detectedVehicleList.append(dv)
            cv2.rectangle(img, (x1,y1),(x2,y2), color, 3)
            text = "{}: {:.4f}".format(object['label'], object['confidence'])
            cv2.putText(img, text, (object['tlx'], object['tly'] - 5), cv2.FONT_HERSHEY_SIMPLEX,
                        0.5, color, 2)

        return img , detectedVehicleList

        cv2.waitKey()

    def predict_multiple(self, images):
        start = time.time()
        print('[INFO] Starting prediction of multiple images')
        results = []
        for i in range(len(images)):
            print('[INFO] Making prediction for image' + str(i))
            result = self.return_predict(images[i])
            results.append(result)
        print('[INFO] Done predicting for all images. TOOK ' + str(int(time.time() - start)))
        return results

    def predict_dummy(self, images):
        results = []
        for i in range(4):
            result = [i for i in range(random.randint(1, 21))]
            results.append(result)
            # print(result)
        return results

if __name__ == '__main__':
    yolo = YOLO()
    imgOriginalScene = cv2.imread("testimages/d2.jpg")
    dimTemp = (800, 650)
    vehicleList = []
    img,vehicleList = yolo.detect_image(imgOriginalScene)
    img = cv2.resize(img, dimTemp, interpolation=cv2.INTER_AREA)
    cv2.imshow("img",img)
    for vehicle in vehicleList:
        print(vehicle.detectPlateNumber())

    cv2.waitKey(0)