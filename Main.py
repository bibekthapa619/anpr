import cv2
import DrawPlate
SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

steps=False
stepsScene=False


def main():
    cap = cv2.VideoCapture('videos/v3.mp4')

    # Check if camera opened successfully
    if (cap.isOpened() == False):
        print("Error opening video  file")

        # Read until video is completed
    while (cap.isOpened()):

        # Capture frame-by-frame
        ret, frame = cap.read()
        # print(time.time()-start_time,"\n")


        if ret == True:

            # Display the resulting frame.


            dimTemp = (800, 650)
            # frame = cv2.resize(f, dimTemp, interpolation=cv2.INTER_AREA)
            cv2.imshow("Frame", frame)

            # Press Q on keyboard to  exit
            if cv2.waitKey(25) & 0xFF == ord('q'):
                break

        # Break the loop
        else:
            break

    # When everything done, release
    # the video capture object
    cap.release()

    # Closes all the frames
    cv2.destroyAllWindows()

if __name__ == "__main__":
    main()
    cv2.waitKey(0);
