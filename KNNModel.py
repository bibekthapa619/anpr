import os
import cv2
import random
import numpy as np
from sklearn.model_selection import  train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib

import ImageProcessFunctions


DATADIR ="images"
CATEGORIES =["0","1","2","3","4","5","6","7","8","9","ba","pa","ga"]
IMG_SIZE=50



def train_model():
    training_data = []
    for category in CATEGORIES:
        path = os.path.join(DATADIR,category)
        class_num=CATEGORIES.index(category)
        for img in os.listdir(path):
            try:
                img_array = cv2.imread(os.path.join(path,img),cv2.IMREAD_GRAYSCALE)
                new_array = cv2.resize(img_array,(IMG_SIZE,IMG_SIZE))
                new_array = ImageProcessFunctions.otsuThreshold(new_array)
                training_data.append([new_array,class_num])
            except Exception as e:
                pass
    random.shuffle(training_data)

    print(len(training_data))

    x=[]
    y=[]

    for features,label in training_data:
        x.append(features)
        y.append(label)

    features  = np.array(x, 'float64')

    nsamples, nx, ny = features.shape
    d2_train_dataset = features.reshape((nsamples,nx*ny))

    X_train, X_test, y_train, y_test = train_test_split(d2_train_dataset, y)

    # train using K-NN
    knn = KNeighborsClassifier(n_neighbors=5)
    knn.fit(X_train, y_train)

    # get the model accuracy
    model_score = knn.score(X_test, y_test)

    print(model_score)
    # save trained model
    joblib.dump(knn, 'models/knn_model.pkl')


if __name__ == "__main__":
    train_model()