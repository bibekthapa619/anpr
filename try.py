import cv2
import numpy as np
import os
import time
import ImageProcessFunctions
import DetectPlates
import Main
import KNNModel
import Preprocess
import DrawPlate

SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)

steps=False
stepsScene=True
charsSegment=True

start_time = time.time()
def main():
    imgOriginalScene = cv2.imread("testimages/a5.jpg")

    if imgOriginalScene is None:  # if image was not read successfully
        print("\nerror: image not read from file \n\n")
        os.system("pause")  # pause so user can see error message
        return
    dimTemp = (800, 650)
    imgOriginalScene = cv2.resize(imgOriginalScene, dimTemp, interpolation=cv2.INTER_AREA)
    imgGrayscale = ImageProcessFunctions.extractValue(imgOriginalScene)
    Preprocess.blurDetection(imgGrayscale)
    x1 = DetectPlates.convertToNonScaledPoint(10,1000,800)
    print(x1)

if __name__ == "__main__":
    main()
    cv2.waitKey(0);
