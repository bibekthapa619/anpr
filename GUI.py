import tkinter
import cv2
import PIL.Image, PIL.ImageTk
import time
import Passage
import yolo
import DBConn

LOCATION = "Pokhara"
def nonScaledPoint(orgX,currentX,x):
    return int(orgX/currentX*x)

def writeLicensePlateCharsOnImage(imgOriginalScene, licPlate,points):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = points
    fontScale = 1
    fontColor = (255, 255, 255)
    lineType = 2

    cv2.putText(imgOriginalScene, licPlate,
                bottomLeftCornerOfText,
                font,
                fontScale,
                fontColor,
                lineType)
    return imgOriginalScene
    # end function
class App:
    def __init__(self, window, window_title, video_source=0):
        self.window = window
        self.window.title(window_title)
        self.video_source = video_source

         # open video source (by default this will try to open the computer webcam)
        self.vid = MyVideoCapture(self.video_source)

         # Create a canvas that can fit the above video source size
        self.canvas = tkinter.Canvas(window, width = self.vid.width, height = self.vid.height)
        self.canvas.pack()

        # Button that lets the user take a snapshot

         # After it is called once, the update method will be automatically called every delay milliseconds
        self.delay = 15
        self.update()

        self.window.mainloop()




    def update(self):
      # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = tkinter.NW)

        self.window.after(self.delay, self.update)


class MyVideoCapture:
    def __init__(self, video_source= 0):
         # Open the video source
        self.vid = cv2.VideoCapture(video_source)
        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

         # Get video source width and
        self.width = 850
        self.height = 600


    def get_frame(self):
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            if ret:
                start_time = time.time()
                 # Return a boolean success flag and the current frame converted to BGR
                # dimTemp = (800, 650)
                # frame = cv2.resize(frame, dimTemp, interpolation=cv2.INTER_AREA)
                dimTemp = (850, 600)
                height,width,chan = frame.shape
                img, vehicleList = yolo.detect_image(frame)
                img = cv2.resize(img, dimTemp, interpolation=cv2.INTER_AREA)
                # cv2.imshow("Frame",newimg)

                for vehicle in vehicleList:
                    bottomx = nonScaledPoint(800,width,vehicle.points[0])
                    bottomy = nonScaledPoint(650, height, vehicle.points[1])

                    if(bottomx < 25):
                        bottomx = 25
                    if(bottomy < 25):
                        bottomy = 25
                    num = vehicle.detectPlateNumber()
                    img = writeLicensePlateCharsOnImage(img,num,(bottomx,bottomy))
                    if(num == ""):
                        continue
                    else:
                        db= DBConn.DBConn()
                        p =Passage.Passage(num,LOCATION)
                        db.addPassage(p)

                    print(num)
                print("Frame processing time:",time.time()-start_time,"  secs")
                return (ret, cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)


     # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

# Create a window and pass it to the App
yolo = yolo.YOLO()
App(tkinter.Tk(), "ANPR","videos/v3.mp4")
