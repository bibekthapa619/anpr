import DrawPlate
class DetectedVehicle:
    def __init__(self, _img, _points):
        self.img = _img
        self.points = _points
        self.number = ""

    def detectPlateNumber(self):
        self.afterDetectionImg , number = DrawPlate.drawPlates(self.img)
        return number

