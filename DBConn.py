import mysql.connector
import Vehicle
import Owner
import Passage
from datetime import datetime

DATABASENAME = "ANPR"
class DBConn:

    def __init__(self):
        try:
            self.mydb = mysql.connector.connect(host = "localhost",user="root",passwd="",database=DATABASENAME)
            self.mycursor = self.mydb.cursor()
        except:
            print("unable to connect to db")

    def getVehicleDetails(self,vNUm):
        try:
            query = "select * from vehicle where num=%s"
            args= (vNUm,)
            self.mycursor.execute(query,args)
            i = self.mycursor.next()
            owner = self.getOwner(vNUm)
            vehicle = Vehicle.Vehicle(vNUm,i[1],i[2],i[3],owner)
            return vehicle
        except:
            return Vehicle.Vehicle()

    def getOwner(self,vNum):
        try:
            query = "select ownID from ownership where vNum=%s"
            args= (vNum,)
            self.mycursor.execute(query,args)
            i = self.mycursor.next()
            ownID = i[0]
            query = "select * from owner where id="+str(ownID)
            self.mycursor.execute(query)
            i = self.mycursor.next()
            owner = Owner.Owner(ownID,i[1],i[2])
            return owner
        except:
            return Owner.Owner()

    def addPassage(self,passage):
        try:
            query = "insert into passage_details values(%s,%s,%s,%s)"
            passage.date = self.getCurrentDate()
            passage.time = self.getCurrentTime()
            args = (passage.vNum,passage.location,passage.date,passage.time)
            self.mycursor.execute(query,args)
            self.mydb.commit()
        except:
            pass

    def getPassageList(self,vNum):
        passageList = []
        try:
            query = "select * from passage_details where vNum=%s"
            args= (vNum,)
            self.mycursor.execute(query,args)
            result = self.mycursor.fetchall()
            for i in result:
                temp = Passage.Passage(i[0],i[1],i[2],i[3])
                passageList.append(temp)
        except:
            pass
        return passageList
    def getPassageListbyLocation(self,location):
        passageList = []
        try:
            query = "select * from passage_details where location=%s"
            args = (location,)
            self.mycursor.execute(query, args)
            result = self.mycursor.fetchall()
            for i in result:
                temp = Passage.Passage(i[0], i[1], i[2], i[3])
                passageList.append(temp)
        except:
            pass
        return passageList

    def getCurrentDate(self):
        return datetime.today().strftime('%Y-%m-%d')
    def getCurrentTime(self):
        return datetime.today().strftime('%H:%M:%S')

if __name__ == "__main__":
    db = DBConn()
    # p =Passage.Passage("a","b","c","d")
    # db.addPassage(p)
    print(str(db.getCurrentTime()))