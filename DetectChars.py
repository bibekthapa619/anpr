import numpy as np
import os
import scipy.ndimage
import cv2
from skimage.feature import hog
from skimage import data, color, exposure
from sklearn.model_selection import  train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.externals import joblib
import DetectPlates
import KNNModel


def img_preprocess(imgOriginal):
    dimTemp = (50, 50)
    imgGrayscale = DetectPlates.getGrayImage(imgOriginal)
    imgGrayscale = cv2.resize(imgOriginal, dimTemp, interpolation=cv2.INTER_AREA)

    return imgGrayscale

def load_model():
    knn = joblib.load('models/knn_model.pkl')
    return knn

def feature_extraction(image):
    return hog(color.rgb2gray(image), orientations=8, pixels_per_cell=(10, 10), cells_per_block=(5, 5))
def predict(image,knn):
    x = []
    x.clear()


    new_array = cv2.resize(image, (50, 50))
    x.append(new_array)

    features = np.array(x, 'float64')
    nsamples, nx, ny = features.shape
    d2_train_dataset = features.reshape((nsamples, nx * ny))

    predictions = knn.predict(d2_train_dataset)[0]
    # print(predictions)
    return predictions

def getNumbersFromPlate(characters):
    knn = load_model()
    plateNum=[]
    for character in characters:
        digit = predict(character.image,knn)
        plateNum.append(matchNumToPlate(digit))
    return plateNum

def matchNumToPlate(digit):
    return str(KNNModel.CATEGORIES[digit])




