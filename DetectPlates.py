# DetectPlates.py

import cv2
import steps
import numpy as np
import math
import copy
import matplotlib.pyplot as plt
import statistics
import DetectChars
import main2
import Character
import NumberPlate
import possibleplate
import ImageProcessFunctions
import Preprocess
import time
# module level variables ##########################################################################


MIN_ASPECTED_RATIO_FRONT = 0.125
MAX_ASPECTED_RATIO_FRONT = 0.4
MIN_ASPECTED_RATIO_BACK = 0.67
MAX_ASPECTED_RATIO_BACK = 1
MIN_PLATE_AREA = (850/15)*(650/15)
MAX_PLATE_AREA = (850/3)*(650/2)
MIN_PLATE_WIDTH = (850/8)
# MIN_PLATE_HEIGHT = 650/15
MAX_ANGLE_ROTATION = 12

###################################################################################################

def getMinArea(h,w):
    return (h/15)*(w/15)
def getMaxArea(h,w):
    return (h/3)*(w/2)
def findPossiblePlates(imgOriginalScene):
    orgHeight , orgWidth , orgChan= imgOriginalScene.shape

    dimTemp = (650, 800)
    imgOriginal = imgOriginalScene
    try:
        imgGrayscale = Preprocess.preprocess(imgOriginal)
    except:
        return []

    height, width = imgGrayscale.shape

    cannyimg = ImageProcessFunctions.findEdges(imgGrayscale)

    # _, imgThresh = cv2.threshold(cannyimg, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)


    closingStructElem = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    closing = cv2.morphologyEx(cannyimg, cv2.MORPH_CLOSE, closingStructElem)

    contours = ImageProcessFunctions.findContours(closing)

    imgcont=np.zeros((height, width, 3), np.uint8)
    cv2.drawContours(imgcont, contours, -1, (255, 255, 255), 2)

    boxcont = copy.deepcopy(imgcont)
    imgcontRatioTest = np.zeros((height, width, 3), np.uint8)

    listofpossibleplates = []
    listofdetectedplates = []
    listofnumberplates = []

    orgcopy= copy.deepcopy(imgOriginal)

    for i in range(0,len(contours)):
        rect = cv2.minAreaRect(contours[i])
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        (x, y), (w, h), angle = rect

        xv,yv,wv,hv = cv2.boundingRect(contours[i])
        if hv>wv:
            continue

        x= (int)(x)
        y=(int) (y)
        w=(int)(w)
        h=(int) (h)

        if w==0 or h==0:
            continue

        aspect_ratio = float(min(w, h) / max(w, h))
        platearea=w*h


        contArea=cv2.contourArea(contours[i])
        if platearea==0 or contArea==0:
            continue

        isAccurate =checkAccuracy(platearea,contArea,0.5)
        heightwidthcheck = (w>MIN_PLATE_WIDTH )


        if ((aspect_ratio<0.4 and aspect_ratio>0.125) or (aspect_ratio>0.4 and aspect_ratio<1))\
                and (platearea>getMinArea(orgHeight,orgWidth) and platearea<getMaxArea(orgHeight,orgWidth) ):

            cv2.drawContours(boxcont, [box], 0, (0, 0, 255), 2)
            cv2.drawContours(imgcontRatioTest, contours[i], -1, (255, 255, 255), 2)
            cv2.drawContours(orgcopy, [box], 0, (0, 255, 0), 2)

            # print("Plate:",platearea,"Cont:",contArea,"angle:",angle)

            rotatedImage = rotateImage(imgOriginal, angle, x, y)
            # rotatedImage = rotateImage(imgOriginalScene, angle,nonScaledPoint(orgWidth,800,x) , nonScaledPoint(orgHeight,650,y))
            y1 = y - (int)(h / 2)
            y2 = y + (int)(h / 2)
            x1 = x - (int)(w / 2)
            x2 = x + (int)(w / 2)
            if h>w:
                rotatedImage=rotateImage(rotatedImage,90,x, y)
                y1 = y - (int)(w / 2)
                y2 = y + (int)(w / 2)
                x1 = x - (int)(h / 2)
                x2 = x + (int)(h / 2)

            tiltedImg=rotatedImage[y1:y2, x1:x2]

            # fixedImg=rotateImage(tiltedImg,-90,0,0)

            listofpossibleplates.append(possibleplate.PossiblePlate(tiltedImg,x1,y1,x2,y2,platearea,box))

    #end for

    # print(len(possiblePlateImg))
    if steps.allSteps and steps.possiblePlateSteps:
        cv2.imshow("Gray image",imgGrayscale)
        # cv2.imshow("Blurred image",imgBlurred)
        cv2.imshow("canny",cannyimg)
        cv2.imshow("closing",closing)
        cv2.imshow("contours",imgcont)
        cv2.imshow("contours box", orgcopy)

    return listofpossibleplates
   
# end function
######################

def nonScaledPoint(orgX,currentX,x):
    return int(orgX/currentX*x)
##################################################################################################

def getCorrectPlates(listofpossibleplates=[]):
    listofnumberplates = []
    if(len(listofpossibleplates)<1):
        return False,[]
    for i in range(0, len(listofpossibleplates)):
        plateDetRes, foundPlate = isPlateCorrect(listofpossibleplates[i], i)
        if (plateDetRes):
            listofnumberplates.append(foundPlate)
    if (len(listofnumberplates) < 1):
        return False,[]
    if (len(listofnumberplates) > 1):
        listofnumberplates = eliminateDuplicatePlates(listofnumberplates)

    if steps.allSteps and True:
        for i in range(0, len(listofnumberplates)):
            listofnumberplates[i].printPlate(i)
            listofnumberplates[i].printChars(i)

    return True,listofnumberplates

##################################################################################################

def eliminateDuplicatePlates(listofnumberplates=[]):
    dupliacatenumberplates=[]
    for i in range(0,len(listofnumberplates)):
        for j in range(i+1,len(listofnumberplates)):

            plate1=listofnumberplates[i]
            plate2=listofnumberplates[j]


            if(plate1.area > plate2.area):
                contRes,containedPlate = isRectangleConatined(plate1,plate2)
            else:
                contRes,containedPlate = isRectangleConatined(plate2,plate1)
            # print("Contained:",contRes)
            if contRes:
                if(len(plate1.characters) < len(plate2.characters)):
                    duplicateplate=plate1
                elif(len(plate1.characters) > len(plate2.characters)):
                    duplicateplate=plate2
                else:
                    duplicateplate=containedPlate
                dupliacatenumberplates.append(duplicateplate)

    nondupliacatenumberplates=list(set(listofnumberplates)-set(dupliacatenumberplates))

    return nondupliacatenumberplates
################################################################################################

def isRectangleConatined(plate1,plate2):
    Ox1 = plate1.x1
    Oy1 = plate1.y1
    Ox2 = plate1.x2
    Oy2 = plate1.y2
    Ix1 = plate2.x1
    Iy1 = plate2.y1
    Ix2 = plate2.x2
    Iy2 = plate2.y2

    dist1= distanceBetweeenPoints(Ox1, Oy1, Ix1, Iy1) < 20
    dist2= distanceBetweeenPoints(Ox2, Oy2, Ix2, Iy2) < 20
    if( (Ox1 <= Ix1 and Oy1 <=Iy1 or dist1) and (Ox2 >= Ix2 and Oy2 >= Iy2 or dist2) ):
        return True,plate2
    else:
        return False,None

###################################################################################################

def distanceBetweeenPoints(x1, y1, x2, y2):
    dist = math.sqrt(pow(x2-x1,2)+pow(y2-y1,2))
    return dist

#####################################################################################################



###################################################################################################


def checkAccuracy(value1,value2,error):
    acc=(float)((max(value1,value2)-min(value1,value2))/max(value1,value2))
    return acc<=error
# end function

def rotateImage(image, angle,x,y):
  image_center = tuple(np.array([x,y]))
  rot_mat = cv2.getRotationMatrix2D(image_center, angle, 1.0)
  result = cv2.warpAffine(image, rot_mat, image.shape[1::-1], flags=cv2.INTER_LINEAR)
  return result
#end function


def getSortedCharForTwoLinedPlates(upperchars = [],lowerchars = []):
    foundChars=[]
    for i in range(0,len(upperchars)):
        for j in range(i,len(upperchars)):
            if(upperchars[i].getDistance() > upperchars[j].getDistance()):
                temp=upperchars[i]
                upperchars[i]=upperchars[j]
                upperchars[j]=temp
    for i in range(0,len(lowerchars)):
        for j in range(i,len(lowerchars)):
            if(lowerchars[i].getDistance() > lowerchars[j].getDistance()):
                temp=lowerchars[i]
                lowerchars[i]=lowerchars[j]
                lowerchars[j]=temp
    foundChars = upperchars+lowerchars
    return foundChars

def getSortedCharForSingleLinedPlates(upperchars = []):
    for i in range(0,len(upperchars)):
        for j in range(i,len(upperchars)):
            if(upperchars[i].getDistance() > upperchars[j].getDistance()):
                temp=upperchars[i]
                upperchars[i]=upperchars[j]
                upperchars[j]=temp
    return upperchars

def isPlateCorrect(possiblePlateOrg,index):

    imgOriginal=copy.deepcopy(possiblePlateOrg.img)
    try:
        imgGrayscale = ImageProcessFunctions.getGrayImage(imgOriginal)
        imgGrayscale = ImageProcessFunctions.maximizeContrast(imgGrayscale)
        height, width = imgGrayscale.shape
        aspectratio = height / width
    except:
        return False,None
    graydup =copy.deepcopy(imgGrayscale)

    _, densitycheckimg = cv2.threshold(imgGrayscale, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    n_white_pix = np.sum(densitycheckimg >= 210)
    whitepixdensity = float(n_white_pix) / (width * height)
    # print("white pix density" + str(index), whitepixdensity)

    if (whitepixdensity > 0.5):
        imgGrayscale = ImageProcessFunctions.invertImage(imgGrayscale)

    imgThresh = ImageProcessFunctions.otsuThreshold(imgGrayscale)
    # cv2.imshow("Img"+str(index),imgThresh)
    contours = ImageProcessFunctions.findContours(imgThresh)

    imgcont = np.zeros((height, width, 3), np.uint8)
    # cv2.drawContours(imgcont, contours, -1, (255, 255, 255), 1)
    cv2.fillPoly(imgcont, contours, color=(255, 255, 255))


    boxcont=copy.deepcopy(imgOriginal)

    possibleCharacters=[]

    foundCharacters = []
    foundUpperCharacters=[]
    foundLowerCharacters = []

#####################################################################################################################
    #for 2 line plate (using aspect ratio)
    if aspectratio>0.4:

        listofupperY=[]
        listoflowerY=[]
        listofupperarea=[]
        listoflowerarea=[]
        for i in range(0,len(contours)):
            rect = cv2.boundingRect(contours[i])

            x, y,w, h = rect

            x = (int)(x)
            y = (int)(y)
            w = (int)(w)
            h = (int)(h)

            if w==0 or h==0:
                continue

            aspect_ratio = (float)(min(w, h) / max(w, h))
            chararea = w * h

            contArea = cv2.contourArea(contours[i])
            if chararea == 0 or contArea == 0 or h> float(height/2) or w> float(width/4):
                continue

            if aspect_ratio>0.4 and chararea>2000 and chararea<(height*width/6):
                midy= int(y+h/2)
                if(midy<int(height/2)):
                    listofupperY.append(midy)
                    listofupperarea.append(chararea)
                else:
                    listoflowerY.append(midy)
                    listoflowerarea.append(chararea)
                possibleCharacters.append(contours[i])

        # print("Possible plate num:",str(index)+":","Upper Chars:",len(listofupperY),"Lower Chars:",len(listoflowerY),"\n")

        if(len(possibleCharacters)<4 or len(listoflowerY)<1 or len(listofupperY)<3):
            return False,None
        upperYavg=statistics.median(listofupperY)
        lowerYavg=statistics.median(listoflowerY)

        upperareaavg=statistics.median(listofupperarea)
        lowerareaavg=statistics.median(listoflowerarea)


        upperChars=0
        lowerChars=0
        for i in range(0,len(possibleCharacters)):
            rect = cv2.boundingRect(possibleCharacters[i])

            x, y, w, h = rect

            x = (int)(x)
            y = (int)(y)
            w = (int)(w)
            h = (int)(h)

            if(int(y+h/2)<int(height/2)):
                avgy=upperYavg
                avgarea=upperareaavg
            else:
                avgy=lowerYavg
                avgarea=lowerareaavg

            midy=int(y+h/2)
            toBound= (max(midy,avgy)-min(midy,avgy))< 20 and checkAccuracy(w*h,avgarea,0.3)
            if (toBound):
                y1=y+h
                x1=x+w
                charImg = imgThresh[y:y1,x:x1]
                tempChar = Character.Character(charImg, x, y)

                wpd = tempChar.getWhitePixDensity()
                if wpd > 0.31:
                    cv2.rectangle(boxcont, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    if (int(y + h / 2) < int(height / 2)):
                        upperChars+=1
                        foundUpperCharacters.append(tempChar)
                    else:
                        lowerChars+=1
                        foundLowerCharacters.append(tempChar)
        if(upperChars<3 or lowerChars<1 or upperChars>4 or lowerChars>4):
            return False,None
        # print("Two lined plate \n")
        # print("Possible plate num", str(index) + ":", "Upper Chars:", upperChars, "Lower Chars:",lowerChars, "\n")
        foundCharacters = getSortedCharForTwoLinedPlates(foundUpperCharacters,foundLowerCharacters)

############################################################################################################
    #for single lined plate
    if aspectratio<0.5:
        listofy=[]
        listofarea=[]
        for i in range(0,len(contours)):
            rect = cv2.boundingRect(contours[i])

            x, y,w, h = rect

            x = (int)(x)
            y = (int)(y)
            w = (int)(w)
            h = (int)(h)

            if w==0 or h==0:
                continue

            aspect_ratio = (float)(min(w, h) / max(w, h))
            chararea = w * h


            contArea = cv2.contourArea(contours[i])
            if chararea == 0 or contArea == 0:
                continue

            y1 = y + h
            x1 = x + w


            if aspect_ratio>0.4 and chararea>600 and chararea<(height*width/4):
                listofy.append(int(y+h/2))
                listofarea.append(chararea)
                possibleCharacters.append(contours[i])
        #endfor
        if (len(possibleCharacters) < 4):
            return False,None
        avgy=float(statistics.median(listofy))
        avgarea=float(statistics.median(listofarea))
        totalchars=0
        for i in range(0,len(possibleCharacters)):
            rect = cv2.boundingRect(possibleCharacters[i])

            x, y, w, h = rect

            x = (int)(x)
            y = (int)(y)
            w = (int)(w)
            h = (int)(h)

            midy= int(y+h/2)

            toBound= (max(midy,avgy)-min(midy,avgy))< 20 and checkAccuracy(avgarea,w*h,0.5)
            # print("avgarea:",avgarea,"area:",w*h)
            if(toBound):
                y1 = y + h
                x1 = x + w
                charImg = imgThresh[y:y1, x:x1]
                tempChar = Character.Character(charImg,x,y)
                wpd = tempChar.getWhitePixDensity()
                if wpd > 0.41:
                    foundCharacters.append(tempChar)
                    cv2.rectangle(boxcont, (x, y), (x + w, y + h), (0, 255, 0), 2)
                    totalchars+=1
        #endfor
        if(totalchars<4 or totalchars>8):
            return False,None
        foundCharacters= getSortedCharForSingleLinedPlates(foundCharacters)

        # print("Single lined plate\n")
        # print("found chars in "+str(index)+":",len(foundCharacters),"\n")
    # if(len(possibleCharacters)<4):
    #     return False

    if steps.allSteps and steps.detectedPlateSteps:
        cv2.imshow("resized plate" + str(index), imgOriginal)
        cv2.imshow("Gray plate"+str(index),graydup)
        cv2.imshow("Binary image" + str(index), imgThresh)
        # cv2.imshow("contour" + str(index), imgcont)
        cv2.imshow("Plate box cont" + str(index), boxcont)

    # for i in range(0,len(foundCharacters)):
    #     cv2.imshow("Num"+str(index)+str(i),foundCharacters[i].image)

    numberPlate=NumberPlate.NumberPlate(imgOriginal,possiblePlateOrg.x1,possiblePlateOrg.y1,possiblePlateOrg.x2,possiblePlateOrg.y2,possiblePlateOrg.area,foundCharacters,possiblePlateOrg.box)

    return True,numberPlate



#end function


#end invertImage












