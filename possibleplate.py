import cv2
import numpy as np
import time

###################################################################################################
class PossiblePlate:

    # constructor #################################################################################
    def __init__(self, _img, _x1, _y1, _x2, _y2, _area,_box=[]):
        self.x1 =_x1
        self.y1 = _y1
        self.x2 = _x2
        self.y2 = _y2
        self.area = _area

        self.x=(self.x1+self.x2)/2
        self.y=(self.y1+self.y2)/2
        self.box=_box
        self.img = _img

        height , width , channel = self.img.shape
        try:
            aspectratio = height / width
            # print(aspectratio)
            if aspectratio < 0.2:
                dim = (400, 100)
            else:
                dim = (400, 300)
            self.img = cv2.resize(self.img, dim, interpolation=cv2.INTER_AREA)
        except:
            pass
    # end constructor

    def getCenter(self):
        return self.x,self.y

    def getArea(self):
        return self.area

    def getBoundingRectPoints(self):
        return self.box

    def getImageFromOriginal(self,orgPic):
        h,w,c = orgPic.shape
        x1 = self.nonScaledPoint(w, 800, self.x1)
        x2 = self.nonScaledPoint(w, 800, self.x2)
        y1 = self.nonScaledPoint(h, 650, self.y1)
        y2 = self.nonScaledPoint(h, 650, self.y2)
        img = orgPic[y1:y2, x1:x2]
        return img

    def nonScaledPoint(self,orgX,currentX,x):
        return int(orgX/currentX*x)
# end class
