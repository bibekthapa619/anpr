import cv2
import numpy as np
import Character
import copy
import DetectChars
import time


class NumberPlate:
    def __init__(self, _img, _x1, _y1, _x2, _y2, _area, _characters=[], _box=[]):
        self.img = _img
        self.characters=copy.deepcopy( _characters)
        self.x1= _x1
        self.y1 = _y1
        self.x2 = _x2
        self.y2 = _y2
        self.x = (self.x1 + self.x2) / 2
        self.y = (self.y1 + self.y2) / 2

        self.area = _area
        self.box = _box

    def printPlate(self,index):
        cv2.imshow("Plate:"+str(index),self.img)

    def printChars(self,index):
        for i in range(0,len(self.characters)):
            cv2.imshow("Chars:"+str(index)+str(i)+str(time.time()),self.characters[i].image)
    def getPlateNumber(self):
        plateNum = DetectChars.getNumbersFromPlate(self.characters)
        return plateNum

    def getBoundingPoints(self):
        return self.box

    def getPrintPostion(self):
        return self.x1,self.y1



