import cv2
import numpy as np
import os
import time
import DrawPlate
import DetectPlates

SCALAR_BLACK = (0.0, 0.0, 0.0)
SCALAR_WHITE = (255.0, 255.0, 255.0)
SCALAR_YELLOW = (0.0, 255.0, 255.0)
SCALAR_GREEN = (0.0, 255.0, 0.0)
SCALAR_RED = (0.0, 0.0, 255.0)


start_time = time.time()
def main():
    imgOriginalScene = cv2.imread("testimages/a5.jpg")

    if imgOriginalScene is None:  # if image was not read successfully
        print("\nerror: image not read from file \n\n")
        os.system("pause")  # pause so user can see error message
        return


    # possibleplates = DetectPlates.findPossiblePlates(imgOriginalScene)
    # result , lisofnumberplates = DetectPlates.getCorrectPlates(possibleplates)
    #
    # for i in range(0,len(lisofnumberplates)):
    #     cv2.drawContours(imgOriginalScene, [lisofnumberplates[i].getBoundingPoints()], 0, (0, 255, 0), 2)
    #     print(lisofnumberplates[i].getPlateNumber(),"\n")

    img,pl = DrawPlate.drawPlates(imgOriginalScene)
    cv2.imshow("Frame", img)
    print("--- %s seconds ---" % (time.time() - start_time))

if __name__ == "__main__":
    main()
    cv2.waitKey(0);

