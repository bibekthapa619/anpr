from tkinter import *
from tkinter import messagebox
from tkinter.ttk import *
import datetime
import time
import DBConn

class App:
    def __init__(self, window, window_title):
        self.window = window
        self.window.title(window_title)
        self.window.geometry("700x500")




        label = Label(self.window, text = 'Search by:')
        label.place( x=5,y=15, anchor = NW)

        OPTIONS = [
            "",
            "Vehicle Num",
            "Location"
        ]

        variable = StringVar(self.window)
        variable.set(OPTIONS[1])  # default value
        self.searchBy = OPTIONS[1]

        self.option = OptionMenu(self.window, variable, *OPTIONS,command=self.setOption)
        self.option.place( x=85,y=15, anchor = NW)

        self.searchtext = Text(self.window,height = 1 ,width = 20)
        self.searchtext.place( x=185,y=15, anchor = NW)

        datelabel = Label(self.window, text='Date:')
        datelabel.place( x=5,y=35, anchor = NW)

        datefromlabel = Label(self.window, text='From:(YYYY-MM-DD)')
        datefromlabel.place(x=65, y=35, anchor=NW)

        self.datefromtext = Text(self.window, height=1, width=20)
        self.datefromtext.place(x=185, y=35, anchor=NW)

        datetolabel = Label(self.window, text='To:(YYYY-MM-DD)')
        datetolabel.place(x=380, y=35, anchor=NW)

        self.datetotext = Text(self.window, height=1, width=20)
        self.datetotext.place(x=490, y=35, anchor=NW)

        self.buttonCommit = Button(self.window, text="GO",command=lambda: self.retrieve_input())
        self.buttonCommit.place(x=300, y=85, anchor=NW)

        frame = Frame(self.window, width=300, height=300)
        frame.grid(row=0, column=0)
        frame.place(x=25,y=115)
        self.treeview = Treeview(frame,selectmode= 'browse')
        self.treeview.grid(columnspan=2)
        self.treeview.pack(side='left',fill= 'x')

        vsb = Scrollbar(frame, orient=VERTICAL, command=self.treeview.yview)
        vsb.pack(side=RIGHT, fill=Y)

        self.treeview.configure(yscrollcommand=vsb.set)

        self.treeview["columns"] = ["Vehicle num", "Location", "Date", "Time"]
        self.treeview["show"] = "headings"
        self.treeview.heading("Vehicle num", text="Vehicle Num")
        self.treeview.heading("Location", text="Location")
        self.treeview.heading("Date", text="Date")
        self.treeview.heading("Time", text="Time")

        self.treeview.column("Vehicle num", width=100, anchor='c')
        self.treeview.column("Location", width=100, anchor='c')
        self.treeview.column("Date", width=100, anchor='c')
        self.treeview.column("Time", width=100, anchor='c')

        self.window.mainloop()

    def setOption(self,value):
        self.searchBy = value

    def validateDate(self,date_text):
        try:
            datetime.datetime.strptime(date_text, '%Y-%m-%d')
        except ValueError:
            return False
        return True

    def retrieve_input(self):
        self.treeview.delete(*self.treeview.get_children())
        searchBy = self.searchBy
        searchValue = self.searchtext.get("1.0", "end-1c").strip()
        if(searchValue == ""):
            messagebox.showerror("Enter "+searchBy, "Field cann't be empty")
            return

        dateFrom = self.datefromtext.get("1.0", "end-1c")
        dateto = self.datetotext.get("1.0", "end-1c")
        if(self.validateDate(dateFrom) == False):
            messagebox.showerror("Invalid date format", "Incorrect data format, should be YYYY-MM-DD")
            return
        if (self.validateDate(dateto) == False):
            messagebox.showerror("Invalid date format", "Incorrect data format, should be YYYY-MM-DD")
            return
        newdate1 = time.strptime(dateFrom, "%Y-%m-%d")
        newdate2 = time.strptime(dateto, "%Y-%m-%d")
        if(newdate1 > newdate2):
            messagebox.showerror("Enter correct date","Date from exceeds date to")
            return

        passageList= []

        db= DBConn.DBConn()
        if(searchBy == "Vehicle Num"):
            passageList = db.getPassageList(searchValue)
        elif(searchBy == "Location"):
            passageList = db.getPassageListbyLocation(searchValue)

        for passage in passageList:
            self.treeview.insert("", 'end', text="L1", values=(passage.vNum, passage.location, passage.date, passage.time))


App(Tk(),"Vehicle Monitoring")