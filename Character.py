import cv2
import numpy as np
import math

class Character:

    def __init__(self,_image,_x,_y):
        _image= cv2.resize(_image,(50,50))
        self.image=_image
        self.x=_x
        self.y=_y

    def getDistance(self):
        dist= float(self.x*self.x+self.y*self.y)
        return dist

    def getWhitePixDensity(self):
        height,width = self.image.shape
        n_white_pix = np.sum(self.image >= 210)
        whitePixDensity = float(n_white_pix) / (width * height)
        return whitePixDensity