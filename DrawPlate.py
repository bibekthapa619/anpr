import cv2
import DetectPlates
import KNNModel
import os
def writeLicensePlateCharsOnImage(imgOriginalScene, licPlate,points):
    font = cv2.FONT_HERSHEY_SIMPLEX
    bottomLeftCornerOfText = points
    fontScale = 1
    fontColor = (255, 255, 255)
    lineType = 2

    cv2.putText(imgOriginalScene, licPlate,
                bottomLeftCornerOfText,
                font,
                fontScale,
                fontColor,
                lineType)
    # end function

def listToString(pl=[]):
    num=""
    for i in range(0 , len(pl)):
        num += pl[i]
    return num
def getResizeDimensions(w,h):
    newh= 700
    neww= int((newh)/h*w)
    return (neww,newh)

def drawPlates(imgOriginalScene):
    if imgOriginalScene is None:  # if image was not read successfully
        print("\nerror: image not read from file \n\n")
        os.system("pause")  # pause so user can see error message
        return
    height ,width, chan = imgOriginalScene.shape
    dimTemp = (700,750)
    imgOriginalScene = cv2.resize(imgOriginalScene, dimTemp, interpolation=cv2.INTER_AREA)

    lisofnumberplates=[]

    possibleplates = DetectPlates.findPossiblePlates(imgOriginalScene)

    result , lisofnumberplates = DetectPlates.getCorrectPlates(possibleplates)
    pl=""
    for i in range(0,len(lisofnumberplates)):
        plateNumber = lisofnumberplates[i].getPlateNumber()
        pl=" "
        print(plateNumber,i,"\n")
        if(checkNumberPlateFormat(plateNumber)):
            points =[lisofnumberplates[i].getBoundingPoints()]
            cv2.drawContours(imgOriginalScene,points , 0, (0, 255, 0), 2)
            pl = listToString(plateNumber)
            writeLicensePlateCharsOnImage(imgOriginalScene,pl,lisofnumberplates[i].getPrintPostion())

    imgOriginalScene = cv2.resize(imgOriginalScene, dimTemp, interpolation=cv2.INTER_AREA)
    return imgOriginalScene,pl

def checkNumberPlateFormat(plateNumber):

    if(KNNModel.CATEGORIES.index(plateNumber[0]) < 10):
        return False
    if (KNNModel.CATEGORIES.index(plateNumber[len(plateNumber)-1]) >= 10):
        return False
    if (KNNModel.CATEGORIES.index(plateNumber[1])>= 10):
        return False

    if (KNNModel.CATEGORIES.index(plateNumber[2]) < 10 and KNNModel.CATEGORIES.index(plateNumber[3]) < 10):
        return False
    if (KNNModel.CATEGORIES.index(plateNumber[2]) >= 10):
        for i in range(3,len(plateNumber)):
            if (KNNModel.CATEGORIES.index(plateNumber[i]) >= 10):
                return False
    if (KNNModel.CATEGORIES.index(plateNumber[3]) >= 10):
        for i in range(4,len(plateNumber)):
            if (KNNModel.CATEGORIES.index(plateNumber[i]) >= 10):
                return False
    return True
