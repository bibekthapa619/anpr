import cv2
import ImageProcessFunctions

def preprocess(imgOriginal):
    imgGrayscale = ImageProcessFunctions.extractValue(imgOriginal)
    # imgGrayscale = cv2.medianBlur(imgGrayscale,3)

    imgGrayscale = ImageProcessFunctions.maximizeContrast(imgGrayscale)

    imgGrayscale = ImageProcessFunctions.blurImage(imgGrayscale)
    return imgGrayscale

def blurDetection(imgGrayscale):
    laplacian_var = cv2.Laplacian(imgGrayscale, cv2.CV_64F).var()
    print("variance:",laplacian_var)

def sharpenImage(imgGrayscale):
    sharpenImg = ImageProcessFunctions.deBlurr(imgGrayscale)
    return sharpenImg